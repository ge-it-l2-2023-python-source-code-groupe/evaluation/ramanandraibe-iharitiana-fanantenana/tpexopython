from variable import friedman,prediction,Resultat
from Affichage import test,poly_A,poly_A_Gc,formatage,formatage2
from Liste import jours_5_et_weekend,jours_5_et_weekend_2,dernier_jours,inverse_jours,saison,mult9,nbrpairs
from boucle_et_comparaison import message,sequ_comp,min_liste,freq_acide,notes_mention,pairs0_20,syracus,struct_secondaire_acide,fibonaci
from Teste import boucleBase1,boucleBase2,boucleBase3, dichotomie,jours_semaine,nbr1_10, nbr_premier,pairs_impairs,moyenne,prod_nbr_cons,triangle,triangle_inverser,triangle_gauche,pyra,parc_matrice_for,parcours_matrice_while,parcours_demi_matrice,saut_puce


def projet():
    choice="1"
    while (choice!="0"):
        print("\n   Menu principal:")
        print("\n   1-________variable_________\n   2-________affichage________\n   3-__________liste__________\n   4-__boucle et comparaison__\n   5-__________test___________\n   6-_________QUITTER_________")
        choice = input("\nVeuillez saisir votre choix:")
        choice=str(choice)
        match choice:
            case "1":
                def variable():
                    print("\n\n\n_______Variable_______\n2.11.1-Nombre de friedman\n2.11.2-Prediction\n2.11.3-Conversion du type\n")
                    choice = input("\nVeuillez saissir le numero de l'exo a executer: ")
                    choice=str(choice)
                    if(choice=="2.11.1"):
                        print("-----2.11.1-----")
                        friedman.friedmancomp()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans variable ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            variable()
                        else:
                            projet()
                    elif(choice=="2.11.2"):
                        print("----2.11.2------")
                        prediction.prediction()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans variable ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            variable()
                        else:
                            projet()
                    elif(choice=="2.11.3"):
                        print("----2.11.3------")
                        Resultat.Resultat()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans variable ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            variable()
                        else:
                            projet()
                    elif(choice=="Entrer"):
                        return
                variable()
            case "2":
                def affichage():
                    print("______Affichage_____\n3.6.1-Affichage resultat de l addition\n3.6.2-Poly-A\n3.6.3-Poly-A et poly G-C\n3.6.4-Ecriture formate\n3.6.5-Ecriture formater2\n")
                    choice = input("Veuillez saisir l'exo a executer: ")
                    choice=str(choice)   
                    if(choice=="3.6.1"):
                        print("---3.6.1---")
                        test.test()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans affichage ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            affichage()
                        else:
                            projet()
                    elif(choice=="3.6.2"):
                        print("---3.6.2---")
                        poly_A.poly_A()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans affichage ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            affichage()
                        else:
                            projet()
                    elif(choice=="3.6.3"):
                        print("---3.6.3---")
                        poly_A_Gc.poly_A_Gc()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans affichage ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            affichage()
                        else:
                            projet()
                    elif(choice=="3.6.4"):
                        print("---3.6.4---")
                        formatage.formatage()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans affichage ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            affichage()
                        else:
                            projet()
                    elif(choice=="3.6.5"):
                        print("---3.6.5---")
                        formatage2.formatage2()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans affichage ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            affichage()
                        else:
                            projet()
                    elif(choice=="MENU"):
                        return
                affichage()
            case "3":
                def liste():
                    print("________liste________\n4.10.1.1-jour de la semaine\n4.10.1.2-jour de la semaine\n4.10.1.3-jour de la semaine\n4.10.1.4-jour de la semaine\n4.10.2-saison\n4.10.3-Table de multiplication\n4.10.4-Nombre paire\n")
                    choice = input("Veuillez saisir l'exo a executer: ")
                    choice=str(choice)
                    if(choice=="4.10.1.1"):
                        print("---4.10.1.1")
                        jours_5_et_weekend.jours_5_et_weekend()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans liste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            liste()
                        else:
                            projet()
                    elif(choice=="4.10.1.2"):
                        print("---4.10.2---")
                        jours_5_et_weekend_2.jours_5_et_weekend_2()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans liste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            liste()
                        else:
                            projet()
                    elif(choice=="4.10.1.3"):
                        print("---4.10.1.3---")
                        dernier_jours.dernier_jours()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans liste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            liste()
                        else:
                            projet()
                    elif(choice=="4.10.1.4"):
                        print("---4.10.1.4---")
                        inverse_jours.inverse_jours()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans liste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            liste()
                        else:
                            projet()
                    elif(choice=="4.10.2"):
                        print("---4.10.2---")
                        saison.saison()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans liste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            liste()
                        else:
                            projet()
                    elif(choice=="4.10.3"):
                        print("---4.10.3---")
                        mult9.mult9()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans liste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            liste()
                        else:
                            projet()
                    elif(choice=="4.10.4"):
                        print("---4.10.4")
                        nbrpairs.nbrpairs()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans liste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            liste()
                        else:
                            projet()
                    elif(choice=="MENU"):
                        return
                liste()
            
            case "4":
                def boucle_et_comparaison():
                    print("______boucle et comparaison______\n5.4.1.1-boucle de base\n5.4.1.2-boucle de base\n5.4.1.3-boucle de base\n5.4.2-Boucle et jour de la semaine\n5.4.3-nombre de 1 a 10 sur une ligne\n5.4.4-nombre paire et impaire\n5.4.5-Calcul de moyenne\n5.4.6-Produit nombre consecutif\n5.4.7-triangle\n5.4.8-triangle inverser\n5.4.9-Triangle gauche\n5.4.10-Pyramide\n5.4.11.1-Parcour de matrice\n5.4.11.2-Parcour de matrice\n5.4.12-parcour de demi matrice\n5.4.13-saut de puce\n5.4.14-suite de fibonacci\n")
                    choice = input("Veuillez saisir l'exo a executer: ")
                    choice=str(choice)
                    if(choice=="5.4.1.1"):
                        print("---5.4.1.1---")
                        boucleBase1.boucleBase1()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.1.2"):
                        print("---5.4.1.2---")
                        boucleBase2.boucleBase2()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.1.3"):
                        print("---5.4.1.3---")
                        boucleBase3.boucleBase3()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.2"):
                        print("---5.4.2---")
                        jours_semaine.jours_semaine()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.3"):
                        print("---5.4.3---")
                        nbr1_10.nbr1_10()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.4"):
                        print("---5.4.4---")
                        pairs_impairs.pairs_impairs()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.5"):
                        print("---5.4.5---")
                        moyenne.moyenne()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.6"):
                        print("---5.4.6---")
                        prod_nbr_cons.prod_nbr_cons()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.7"):
                        print("---5.4.7---")
                        triangle.triangle()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.8"):
                        print("---5.4.8---")
                        triangle_inverser.triangle_inverser()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.9"):
                        print("---5.4.9---")
                        triangle_gauche.triangle_gauche()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.10"):
                        print("---5.4.10---")
                        pyra.pyra()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.11.1"):
                        print("---5.4.11.1---")
                        parc_matrice_for.parc_matrice_for()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.11.2"):
                        print("---5.4.11.2---")
                        parcours_matrice_while.parcours_matrice_while()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.12"):
                        print("---5.4.12---")
                        parcours_demi_matrice.parcour_demi_matrice()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                        
                    elif(choice=="5.4.13"):
                        print("---5.4.13---")
                        saut_puce.saut_puce()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="5.4.14"):
                        print("---5.4.14---")
                        fibonaci.fibonaci()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans boucle et comparaison ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            boucle_et_comparaison()
                        else:
                            projet()
                    elif(choice=="Entrer"):
                        return
                boucle_et_comparaison()
            case "5":
                def Teste():
                    print("_______test______\n6.7.1-jour de la semaine\n6.7.2-Sequence complementaire d'un brin d'ADN\n6.7.3-Minimum d'une liste\n6.7.4-Fréquence des acides aminés\n6.7.5-Notes et mention d'un étudiant\n6.7.6-Nombres pairs\n6.7.7-Conjecture de Syracuse\n6.7.8-Attribution de la structure secondaire des acides aminés d'une protéine (exercice +++)\n6.7.9-Détermination des nombres premiers inférieurs à 100\n6.7.10-Recherche d'un nombre par dichotomie\n")
                    choice = input("Veuillez saisir l'exo a executer: ")
                    choice=str(choice)
                    if(choice=="6.7.1"):
                        print("---6.7.1---")
                        message.message()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans teste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            Teste()
                        else:
                            projet()
                    elif(choice=="6.7.2"):
                        print("---6.7.2---")
                        sequ_comp.sequ_comp()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans teste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            Teste()
                        else:
                            projet()
                    elif(choice=="6.7.3"):
                        print("---6.7.3---")
                        min_liste.min_liste()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans teste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            Teste()
                        else:
                            projet()
                    elif(choice=="6.7.4"):
                        print("---6.7.4---")
                        freq_acide.freq_acide()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans teste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            Teste()
                        else:
                            projet()
                    elif(choice=="6.7.5"):
                        print("---6.7.5---")
                        notes_mention.notes_mention()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans teste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            Teste()
                        else:
                            projet()
                    elif(choice=="6.7.6"):
                        print("---6.7.6---")
                        pairs0_20.pairs0_20()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans teste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            Teste()
                        else:
                            projet()
                    elif(choice=="6.7.7"):
                        print("---6.7.7---")
                        syracus.syracus()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans teste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            Teste()
                        else:
                            projet()
                    elif(choice=="6.7.8"):
                        print("---6.7.8---")
                        struct_secondaire_acide.acide_amine_d_un_proteine()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans teste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            Teste()
                        else:
                            projet()
                    elif(choice=="6.7.9"):
                        print("---6.7.9---")
                        nbr_premier.nbr_premier()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans teste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            Teste()
                        else:
                            projet()
                    elif(choice=="6.7.10"):
                        print("---6.7.10---")
                        dichotomie.dichotomie()
                        reponse=input("\nVoulez vous selectionner un autre exercice dans teste ou retourner au menu principal(a pour autre ou r pour retour):")
                        if reponse=="a":
                            Teste()
                        else:
                            projet()
                    elif(choice=="Entrer"):
                        return
                Teste()        
            case "6":
                break
projet()


