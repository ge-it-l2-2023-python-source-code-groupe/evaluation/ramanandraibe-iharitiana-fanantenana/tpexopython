def saison():
    hiver = ['décembre', 'janvier', 'février']
    printemps = ['mars', 'avril', 'mai']
    été = ['juin', 'juillet', 'août']
    automne = ['septembre', 'octobre', 'novembre']

    saisons = [hiver, printemps, été, automne]
    print("Saison[2]: ",saisons[2])
    print("Saison[1][0]: ",saisons[1][0])
    print("Saison[1:2]: ",saisons[1:2])
    print("Saison[:][1]: ",saisons[:][1])