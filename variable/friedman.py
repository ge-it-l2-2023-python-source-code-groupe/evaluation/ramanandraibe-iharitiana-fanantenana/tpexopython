def friedmancomp():
    def friedman(expression, resultat_attendu):
        try:
            resultat_calcul = eval(expression)
            return resultat_calcul == resultat_attendu
        except ZeroDivisionError:
            return False

    def tester_nombre_friedman(expression, resultat_attendu):
        if friedman(expression, resultat_attendu):
            print(f"Test d'expression mathématique:\nL'expression {expression}={resultat_attendu} --> est un nombre de Friedman")
        else:
            print(f"Test d'expression mathématique:\nL'expression {expression}={resultat_attendu} --> n'est pas un nombre de Friedman")

    # Tester les expressions
    tester_nombre_friedman("7+3**6", 736)
    tester_nombre_friedman("(3+4)**3", 343)
    tester_nombre_friedman("3**6-5", 728)
    tester_nombre_friedman("(1+2**8)*5", 1285)
    tester_nombre_friedman("(2+1**8)**7", 2187)