def sequ_comp():
    ADN=['A', 'C', 'G', 'T', 'T', 'A', 'G', 'C', 'T', 'A', 'A', 'C', 'G']
    print("Sequence d'un brin d'ADN:")
    print(ADN)
    print("Sa sequence complementaire est:")
    for i in ADN:
        if i=="A":
            ADN="T"
        elif i=="T":
            ADN="A"
        elif i=="G":
            ADN="C"
        else:
            ADN="G"
        print( ADN ,end="  ")