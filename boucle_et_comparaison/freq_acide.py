def freq_acide():
    acideamines=["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]
    fA,fR,fW,fG=0,0,0,0
    listA,listR,listW,listG=[],[],[],[]
    for i in acideamines:
        if i =="A":
            fA+=1
        elif i=="R":
            fR+=1
        elif i=="W":
            fW+=1
        else:
            fG+=1
        listA.append(fA)
        listR.append(fR)
        listW.append(fW)
        listG.append(fG)
    print(f"La frequence de A est de: {listA.pop()}\nLa frequence de R est de: {listR.pop()}")
    print(f"La frequence de W est de: {listW.pop()}\nLa frequence de G est de: {listG.pop()}")
