def pairs0_20():
    nbr=list(range(0,21,1))
    print("Les nombres pairs inferieur ou egals a 10 sont:")
    for i in nbr:
        rest=i%2
        while rest==0 and i<=10:
            print(i,end=" ")
            break
    print("\nLes nombre impairs superieur a 10 sont:")
    for i in nbr:
        rest=i%2
        while rest!=0 and i>10:
            print(i,end=" ")
            break