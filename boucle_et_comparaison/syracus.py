def syracus():
    n = 10
    syracuse = [n]
    trivial = []
    for i in range(n):
        while n != 1 :
            print(n, end=", ")
            if n % 2 == 0 :
                n = n // 2
            elif n % 2 != 0 :
                n = 3 * n + 1
        syracuse += [n]
        if n == 1 :
            trivial = syracuse[-3:0] 
    print(f"\nle nombre trivial est : {syracuse}")
