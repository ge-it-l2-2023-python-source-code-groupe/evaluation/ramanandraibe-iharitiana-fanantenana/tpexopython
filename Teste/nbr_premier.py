def nbr_premier():
    def premier(n):
        if n<2:
            return False
        for i in range(2,int(n**0.5)+1):
            if n%i==0:
                return False
        return True
    nombres_premier=[n for n in range(101)if premier(n)]
    nombre_de_premier=len(nombres_premier)
    print(f"Il y a {nombre_de_premier} nombres premier entre 0 a 100")
