def dichotomie():
    def dichotom():
        print("Pensez à un nombre entre 1 et 100.")
        min, max = 1, 100
        questions = 0

        while True:
            proposition = (min + max) // 2
            reponse = input(f"Est-ce que votre nombre est plus grand, plus petit, ou égal à {proposition}?:[+ ou - ou =]")

            if reponse == '+':
                min = proposition + 1
            elif reponse == '-':
                max = proposition - 1
            elif reponse == '=':
                print(f"J'ai trouvé en {questions} questions !")
                break
            else:
                print("Veuillez entrer +, - ou =.")

            questions += 1
            break
    dichotom()