def pyra():
    reponse = input("Entrez un nombre de lignes (entier positif): ")
    N = int(reponse)
    for i in range(N):
        print(" "*(N-i)+"*"*(i)+"*"*(i+1))